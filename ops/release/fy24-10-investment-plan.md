# FY24 Release -10% investment plan

What impact would a -10% investment in FY24 have on the Release direction? What impact would 1.5 headcount decrease have on the Release direction?

Note: The following page may contain information related to upcoming products, features and functionality. It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes. Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features or functionality remain at the sole discretion of GitLab Inc. 

## Context

Currently, [the Release team](https://about.gitlab.com/handbook/engineering/development/ops/release/) is fully staffed with 8 engineers (6 BE, 2 FE), TW, EM, PD, PM and UX manager. A decreased 10% investment would result in 6.5 engineers (5 BE, 1.5 FE)

In FY23, our focus was mainly on Environments management, to help customers better understand and manage their deployments to various environments and deployment execution functionality, like deploy approvals. We also focused heavily on reliability, usability, and scalability throughout the year for many of our features. And though we did not achieve maturity upgrades on other categories, we were still able to make minor improvements to Release (the feature) workflows and maintainance and bug fixes for Release Orchestration and Continuous Delivery categories.

## Proposal

The decreased investment would require us to cut scope in the roadmap.

### Planned progress without lost investment

See the [flat investment plan](./fy24-flat-investment-plan.md).

### The effect of the lost investment

### Category Focus
- Environment Management
- Release Orchestration (slower progress)
- Supporting Configure Kubernetes Management

### Major Themes

We would likely cut the priorities below:
1. Changelogs
1. Coordinating Releases/deployments across projects
1. Usability improvements (Not explicitly called out in the flat plan but highlighting here that if we had reduced capacity, we would not take on usability improvement commitments)

and keep:
1. Environments integration with K8s Agent
1. Group Environments
1. Deployment set up (overlap with configure) - similar to how Harness has a whole UI to configure deployments
1. Improved rollback experience
1. Group Releases


### Effect on product metrics

- Release SMAU. Target: 900k. Effect: -100k
- Deploy Approval MAU. Target: 5k. Effect: -1k
- Number of monthly deployments. Target: 100M. Effect: -15M

| Metric | FY24 Start | MoM % growth assumption (change from flat plan) | Q1 Target | Q2 Target | Q3 Target | Q4 Target / FY24 End |
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| Release SMAU | 588k | 2.5% (-.5%) | 633k | 681k | 734k | 790k |
| Deploy Approval MAU | 1.2k | 10% (no change) | 1.6k | 2.1k | 2.8k | 3.8k |
| Number of monthly deployments | 73.7M | 2.5% (-.5%) | 79M | 86M | 92M | 99M |
