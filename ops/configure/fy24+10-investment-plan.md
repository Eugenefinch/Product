# FY24 Configure +10% investment thought experiment

What impact would a +10% investment in FY24 have on the Configure direction? What impact would 1.5 headcount increase have on the Configure direction?

Note: The following page may contain information related to upcoming products, features and functionality. It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes. Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features or functionality remain at the sole discretion of GitLab Inc. 

## Context

Right now, [the Configure team](https://about.gitlab.com/handbook/engineering/development/ops/configure/) is fully staffed with 8 engineers (7 BE, 1 FE), TW, EM, PD, PM and UX manager. As a result, any extra headcount is supposed to result in a team split. I'll assume no team split happens.

In FY23, our focus was slowly shifting from [Kubernetes management](https://about.gitlab.com/direction/configure/kubernetes_management/) to [deployment management](https://about.gitlab.com/direction/configure/deployment_management/). Deployment management is a main pillar of our [Delivery direction](https://about.gitlab.com/direction/delivery/).

## Proposal

The additional headcount would be used to speed up our work on Deployment management direction.

### Planned progress without additional investment

See the [flat investment plan](./fy24-flat-investment-plan.md).

### The effect of additional investment

The additional investment would allow us to move [GitOps support from Viable to Lovable](https://gitlab.com/gitlab-org/configure/general/-/issues/321). The flat investment plan targets Viable maturity GitOps support and lists the features that block the removal of the certificate-based integration. As both plans assume a move towards Flux, our solution would automatically receive support for many Complete features and we would need to focus on the integrations only. As a result, the additional investment would allow us to fill the remaining gaps and get to Lovable.

Moreover, we would be able to speed up the development of [the GitLab Delivery framework](https://about.gitlab.com/direction/configure/deployment_management/#gitlab-delivery)

### Effect on product metrics

- Adoption of the agent on SaaS
- Adoption of the agent on Self-Managed
- MAU of GitOps usage
- MAU of CI/CD sharing
- MAU of Kubernetes dashboard usage
- Adoption of the GitLab deployment pipeline on SaaS
- Adoption of the GitLab deployment pipeline on Self-Managed

| Topic | Starting | Q1 change/reference | Q2 | Q3 | Q4 |
|---------|---------|-----|-----|-------|---------|
| [CI/CD MAU](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=15256566&udv=1012098) | 8000* | +1K/10,000 | +1K/22,000 | +1K/26,000 | +1K/30,000 |
| [Number of active agents (SaaS)](https://dashboards.gitlab.net/d/kas-main/kas-overview?orgId=1&viewPanel=28) | 7,400** | +2K/15,000 | +4K/25,000 | +10K/35,000 | +10K/43,000 |
| [Number of active agents (SM)](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=16032187&udv=1012098) | 11,800** | 0/21,000 | +2K/36,000 | +2K/50,000 | +4K/67,000 |
| [Percentage of SM instances with an agent (Premium)](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=9639658&udv=1012098) | 0/11%** | 0/13% | +1%p/15% | +1%p%17% | +2%p/20% |
| [GitOps MAU](https://gitlab.com/gitlab-org/gitlab/-/issues/366294) | | X | ? | ? | ? |

* \* - estimated value
* ** - November value
