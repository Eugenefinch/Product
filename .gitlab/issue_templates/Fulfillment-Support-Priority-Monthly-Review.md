## Background

L&R Support labels Fulfillment issues using the labels ~"Support Priority::1" through ~"Support Priority::4". This issue is to complete a monthly review in which, ahead of each milestone, each PM reviews the Support priority items and provides updates on the status of the top priority items for their respective groups. 

## Priority issues board

1. Support Priority Epics Board: https://gitlab.com/groups/gitlab-org/-/epic_boards/39981?label_name%5B%5D=devops::fulfillment
1. Support Priority Issues Board: https://gitlab.com/groups/gitlab-org/-/boards/2543339?label_name%5B%5D=section::fulfillment


## Opening tasks

- [ ] @ofernandez2 start retro thread in this issue
- [ ] @ofernandez2 set a due date

## Reviews by Group

- [ ] Provision - Review support priority items and provide updates - @courtmeddaugh ([epics](https://gitlab.com/groups/gitlab-org/-/epic_boards/39981?label_name[]=group%3A%3Aprovision&label_name[]=Support%20Priority), [issues](https://gitlab.com/groups/gitlab-org/-/boards/4356177?label_name[]=group%3A%3Aprovision&label_name[]=Support%20Priority))
- [ ] Billing and Subscription Management - Review support priority items and provide updates - @tgolubeva ([epics](https://gitlab.com/groups/gitlab-org/-/epic_boards/39981?label_name[]=devops%3A%3Afulfillment&label_name[]=Support%20Priority&label_name[]=group%3A%3Asubscription%20management), [issues](https://gitlab.com/groups/gitlab-org/-/boards/2543339?label_name[]=section%3A%3Afulfillment&label_name[]=group%3A%3Asubscription%20management))
- [ ] Purchase - Review support priority items and provide updates - @ppalanikumar
- [ ] Utilization - Review support priority items and provide updates - @alex_martin
- [ ] Fulfillment Platform - Review support priority items and provide updates - @tgolubeva ([epics](https://gitlab.com/groups/gitlab-org/-/epic_boards/39981?label_name%5B%5D=devops%3A%3Afulfillment&label_name%5B%5D=Support%20Priority&label_name%5B%5D=group%3A%3Afulfillment%20platform), [issues](https://gitlab.com/groups/gitlab-org/-/boards/2543339?label_name%5B%5D=section%3A%3Afulfillment&label_name%5B%5D=group%3A%3Afulfillment%20platform))

### Format for each group update

```
## GROUP_NAME Review

**Legend**

* :white_check_mark: being actively worked on
* :large_orange_diamond: planned / being worked on but at risk
* :x: not being worked on

**EPIC**

1. :x: ~"Support Priority::1" `LINK_TO_EPIC`. `NOTES`.
1. :white_check_mark: ~"Support Priority::2" `LINK_TO_EPIC`. `NOTES`.

**ISSUE**

1. :large_orange_diamond: ~"Support Priority::1" `LINK_TO_ISSUE`. `NOTES`.
1. :x: ~"Support Priority::2" `LINK_TO_ISSUE`. `NOTES`.

**Closed Issued or Epics**

1. Highlight any closed issues / epics that support was tracking since last month

cc `@engineering-manager`
```

## Closing tasks

- [ ] @ofernandez2 review that updates have been completed and tag support (`@gitlab-com/support/licensing-subscription`) for their review.
- [ ] Support reviews updates and provide any feedback, questions, concerns.
- [ ] Make adjustments to template as needed https://gitlab.com/gitlab-com/Product/-/tree/main/.gitlab/issue_templates/Fulfillment-Support-Priority-Monthly-Review.md
- [ ] Close out issue

/assign @ofernandez2 @tgolubeva @courtmeddaugh @alex_martin @ppalanikumar

/label ~"section::fulfillment" ~"Fulfillment Support Priority Review"