## Intent 🎯
Monthly review of JiHu milestone plan to create awareness and alignment between JiHu and effected product groups


## Tasks ✅
- [ ] Link JiHu Milestone plan issue 
- [ ] Ensure EM/PM/PD are tagged in planned upstream issues
- [ ] Check downstream issues
- [ ] Post update in [#product](https://gitlab.slack.com/archives/C0NFPSFA8) and repost in [#jihu-product](https://gitlab.slack.com/archives/C01S8CFF7HR)

/assign @kbychu

cc @meks
