## Introduction

After each release it's important to update your category and stage visions to ensure they accurately reflect what was delivered, especially in the `What's Next` sections. You should also consider this an opportunity to make any updates that you might have been considering  but not put the time into creating an MR for. Consider creating a branch and reviewing each of your Direction pages (Stage and Categories) one-by-one looking for updates.

Be sure to follow the most up-to-date guidance in the handbook about [managing your product direction](https://about.gitlab.com/handbook/product/product-management/process/#managing-your-product-direction).

Check the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) for the due date for this content.

## Tasks

Submit and merge MRs for any needed updates. If there are updates, post them in the appropriate channels (#s_, #g_)

### :o: Opening Tasks
* [ ] @jreporter - Add Retrospective Thread to this issue 
* [ ] @jreporter - Assign a due date to this issue
* [ ] @jreporter - If first month of the quarter - remind team to perform a [Quarterly Pricing Tier strategy review](https://about.gitlab.com/handbook/product/categories/ops/#quarterly-pricing-strategy-review)

### :telescope: Update Group Categories and Related Themes

For example the direction/verify/continous_integration or direction/monitor/health/incident_management pages.

Note: Before submitting for review or merging yourself, be sure to:
- Consider whether you can separate out controversial and non-controversial updates to focus discussion and feedback on specific changes
- Look at your page in the review app to make sure that it looks like you expect and you don't have markdown errors
- Run a spell check and/or grammar checker
- Open all your links and make sure none go to issues you've already delivered/moved or are otherwise dead links
- Review and update the entire document, not just the what's next section
- For controversial or material updates ping your Stage, Section and Product Leaders to provide a global perspective during review

* [ ] @jreporter  - Verify:Pipeline Execution - Category visions reviewed
* [ ] @dhershkovitch - Verify:Pipeline Authoring - Category visions reviewed
* [ ] @DarrenEastman - Verify:Runner - Category visions reviewed
* [ ] @gabrielengel_gl - Verify:Runner SaaS - Category visions reviewed
* [ ] @jocelynjane - Verify:Pipeline Security - Category visions reviewed
* [ ] @trizzi - Package:Package - Category visions reviewed


### :chart_with_upwards_trend: Review Maturity Plans

Check each category is listed and up to date on the [Maturity](https://about.gitlab.com/direction/maturity/) page.
Category Maturity Plans can be updated by modifying [categories.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/categories.yml).

Reminder: changes to Maturity Plans require review!

* [ ] @jreporter - Verify:Pipeline Execution - Maturity plans updated
* [ ] @dhershkovitch - Verify:Pipeline Authoring - Maturity plans updated
* [ ] @DarrenEastman - Verify:Runner - Maturity plans updated
* [ ] @gabrielengel_gl - Verify:Runner SaaS - Maturity plans updated
* [ ] @jocelynjane - Verify:Pipeline Security - Maturity plans updated
* [ ] @trizzi - Package:Package - Maturity plans updated


### :joystick: Record New Speed Runs

* [ ] @jreporter - Verify:Pipeline Execution - Speed run recorded
* [ ] @dhershkovitch - Verify:Pipeline Authoring - Speed run recorded
* [ ] @DarrenEastman - Verify:Runner - Speed run recorded
* [ ] @gabrielengel_gl - Verify:Runner SaaS - Speed run recorded
* [ ] @jocelynjane - Verify:Pipeline Security - Speed run recorded
* [ ] @trizzi - Package:Package - Speed run recorded


### :writing_hand: Update Stage Content
Review stage direction pages including tiering strategy content. Be sure to incorporate appropriate [pricing themes](https://about.gitlab.com/company/pricing/#themes) in that review.  On a quarterly basis consider scheduling a Direction review discussion with Product leaders to solicit a global perspective on your stage Direction.  
* [ ] @jreporter - Verify - Stage Direction Updated including making Letters from the Editor evergreen
* [ ] @trizzi - Package - Stage Direction Updated including making Letters from the Editor evergreen


### Update Section Content
Review and update section direction pages including tiering strategy content, update section walk-through
* [ ] @jreporter - Update Section Direction page
* [ ] @jreporter - Update Section Walk Through
* [ ] @jreporter - Quarterly - consider scheduling a section-wide direction review and encouraging all Gitlab team members to contribute

### Update Exciting Things and Accomplishments (Experimental within the Ops Section)
Consider updating your Exciting Things and Accomplishments sections of your group handbook pages. 
Consider maintaining no more than three months worth of accomplishments.

* [ ] @jreporter - Verify:Pipeline Execution - Group handbook page updated
* [ ] @dhershkovitch - Verify:Pipeline Authoring - Group handbook page updated
* [ ] @DarrenEastman - Verify:Runner - Group handbook page updated
* [ ] @jocelynjane - Verify:Pipeline Security - Group handbook page updated
* [ ] @trizzi - Package:Package - Group handbook page updated
* [ ] @jreporter - CI - Update Section Content

### :x: Closing Tasks
* [ ] @jreporter - Review [category maturity plans](https://about.gitlab.com/direction/maturity/) for the next three months
* [ ] @jreporter - Review all updates
* [ ] @jreporter - Post Highlights comment on this issue upon reviewing all updates and communicate widely (ping team, post in slack in #ops-section #customer-success and #product channels, ping `@gl-product-leadership` `@gl-cd-section-pms`, `@gl-ci-section-leaders`)
* [ ] @jreporter - Follow up on retrospective thread activities with [updates to this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/CI-Section-Direction-Updates.md)

FYI - @rayana @mvanremmerden

/assign @jreporter @trizzi @DarrenEastman @dhershkovitch @jocelynjane @mflouton

/label ~"Direction Update" ~Doing
