Hello Product Leads-- Please take the time to sign up, coordinate and reach out to <%= FiscalDateHelper.new(Date.today).this_quarter %> PNPS users who opted to chat with us! Please note this list is <%= FiscalDateHelper.new(Date.today).this_quarter %> paid SaaS users only. Readout deck `[here](ADD LINK)`. 

You can sign up (or assign your GPMs or PMs) using `[QX PNPS Followup Users](ADD LINK)`. Please note restricted access to only `product@gitlab.com` and `timtams@gitlab.com` due to privacy of user contacts. 

The process to follow is the same as last quarter and can be found in the product handbook: [PNPS Responder Outreach](https://about.gitlab.com/handbook/product/product-processes/#pnps-responder-outreach). Please note we ask that you add the label `PNPS Improvement` to any epics/issues that result from the PNPS analysis and/or PNPS outreach calls.  

When you conduct the interviews, please record the sessions and post to the [PNPS/SUS Follow Up project in Dovetail](https://gitlab.dovetailapp.com/projects/36nmGVKvkaT7SGMXtUeHVg/v/70xPTo5RzTRZnCNEVz1fWH) under the `PNPS` column. There is a [template](https://gitlab.dovetailapp.com/data/515bL0PxJlOFqS7RcFWL4l) available to use for conducting interviews within Dovetail. Please make a copy of the template when adding your interview data to the project.

**Important note**: We do not want to overwhelm users with multiple contacts. If the person you want to talk to has already been signed up for by someone else for outreach, coordinate to join your questions and/or outreach session. Thank you!

/assign @wleidheiser
/confidential

*To Do (for UX Research only)*

PNPS survey research issue for this quarter: `Add link to current quarter's PNPS issue here`

- [ ] Add `:recycle: Retrospective Thread` as a comment 
- [ ] Create the PNPS follouwp user list with restricted access to only `product@gitlab.com` and `timtams@gitlab.com` ([sample](https://docs.google.com/spreadsheets/d/1SjH6dWOKVo1du_ou4YpUSfLjHf5FIDTeER0F43_kg6I/edit#gid=132919894)) 
- [ ] Update content and all document links in issue description as needed 
- [ ] Add due date: 5th day of last month of the current fiscal quarter
- [ ] Add milestone
- [ ] Assign to appropriate epic `FYXX PNPS Surveys`
- [ ] Tag in `@gl-product` to action 
- [ ] Tag in `@spatching` to action TAMs 
- [ ] 2 weeks prior to due date: 
     - [ ] Change workflow label to `workflow::inreview`
     - [ ] Check user list to see if team has actioned sufficiently, and if not, ping product leads to action
- [ ] Update [issue template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PNPS-Responder-Outreach.md) with any items from the Retrospective thread 
- [ ] Review and close issue on due date 
