<!--- https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/core-platform-Section-PI-Updates.md --->

## :book: References

* [Handbook Page](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/core-platform-section/)
* [Meeting Notes](https://docs.google.com/document/d/1HxJcX99nh3aRHuUM7aGkgPGORtj-IzTZtrYTDxmd5pA/edit)
* [Calendar link](https://calendar.google.com/calendar/u/0/r/search?q=Performance%20Indicator%20Meeting%3A%20Enablement%20(monthly)%20%5BREC%5D&sf=true)

## :dart: Intent
We are organizing for and regularly reviewing Performance Indicators (PIs) in order to enable a dialog between ourselves (all of R&D) about most effective use of our R&D efforts towards the most impactful improvements. As a result we should:

*  focus on singular [Performance Indicators](https://about.gitlab.com/handbook/product/metrics/#north-star-metric)
*  focus on the art of the possible, what we can measure and react to quickly
*  focus on business metrics, particularly [GMAU](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/#group-monthly-active-users-gmau) or your PPI
*  continue beyond measurement and review of our Performance Indicators to building a [Growth Model](https://about.gitlab.com/handbook/product/metrics/#example-1-north-star-metrics-breakdown), a [Growth funnel](https://about.gitlab.com/handbook/product/metrics/#aarrr-framework-as-a-north-star-funnel) and supporting metrics and segmentation

As a communication tool, Performance Indicators are only as useful as the range of the audience. As a result we should:

* store PI metrics, funnels, growth models, status and next steps handbook first (avoid content only living in slides)
* expose our product groups to PI metrics regularly
* communicate transparently about adjustments to PIs within our groups
* reference PIs in planning issues and prioritization discussions

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Set a due date to this issue as 3 business days prior to the scheduled review - @joshlambert
* [ ] Add a retrospective thread to this issue - @joshlambert

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Performance Indicators YAML file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/main/data/performance_indicators/core_platform_section.yml) with:

* Update your `health:` and `instrumentation:` levels
* Update your `monthly_focus.goals` to indicate what next month's focus area will be
* Update any `urls:` to reference additional SiSense dashboards for more details
* Update any `urls:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update `health:` `reasons:` with `Insight -` prefixed comments about insights gained from the last month of data
  *  Consider any leading indicators. Like version migrations and net new growth
  *  Consider what specific changes directly impact how your PI is measured. Like spam or a large customer onboarding 
* Update `health:` `reasons:` with `Improvement -` prefixed comments about planned improvements to reach your goals over the next month
* If there have been recent updates to the definition, rationale, growth model, funnel highlight those and include those as `reasons:` under `health:`
* If you don't have a defined metric, rationale, growth model or funnel in the handbook reference issues for creating those and provide status on blockers in the `reasons:` under `instrumentation:`.

#### Updates

Product:
- [ ] ~"devops::core-platform" @joshlambert - [MR Link]()
- [ ] ~"group::cloud connector" @rogerwoo - [MR Link]()
- [ ] ~"group::database" @rogerwoo - [MR Link]()
- [ ] ~"group::distribution" @dorrino  - [MR Link]()
- [ ] ~"group::geo" @sranasinghe  - [MR Link]()
- [ ] ~"group::global search" @bvenker - [MR Link]()
- [ ] ~"group::gitaly" @mjwood - [MR Link]()
- [ ] ~"group::tenant scale" @lohrc - [MR Link]()

Quad Review:
- [ ] Engineering -  @cdu1
- [ ] Quality - @ksvoboda
- [ ] UX - @chrismicek

### :x: Closing Tasks
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Core-Platform-Section-PI-Updates.md) based on the retrospective thread - @joshlambert
* [ ] Activate on any retrospective thread items - @joshlambert

/assign @chrismicek @tauriedavis
