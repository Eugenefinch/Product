<!-- Please refer to [product manager onboarding](https://about.gitlab.com/handbook/product/product-manager-role/#product-manager-onboarding) for guidance on using this template -->
Welcome to GitLab and the `<INSERT GROUP>` `<INSERT NAME>`!

<!-- TODO: This intro content and through this template can be personalized if the hiring manager chooses to do so. We do not require but highly recommend rolling the goals identified for each milestone below into the PM's first CDF review 

**Notes for hiring manager** 

- We want product managers to work on the Strategic items below during their first _full_ 3 milestones at GitLab. It's recommended that you plug in the milestones and dates in this issue as the PM will not yet be familiar enough with our milestone cadence. Depending on the PM's start date, the third milestone may fall past 100 days, which is ok.
- It's recommended you do an initial review and shaping of the Strategic milestone-driven goals to give the PM some guide rails but encourage them to engage with you and make it their own as needed. 

** Notes for onboarding buddy**
You may find the template for buddy tasks here useful: https://gitlab.com/-/snippets/2572026
-->

We are so happy and excited that you decided to join us. Starting a new job can be daunting. There will be things to learn, relationships to build, and adjustments to new ways of doing things. We want you to know that we've got your back! Take your time, ask questions, ask for help, say I don't know. Doing that is perfectly ok! Remember, you've already impressed us. You do not need to feel immediate pressure to "prove your worth" just as you are getting started.

This issue provides some guide rails on what you should focus on in your first 100 days on the job. The Tactical tasks are standards and should be completed as is. The Strategic tasks are for you and your manager to shape together. 

## First 4 weeks
Get the clerical tasks out of the way so you've got everything you need to do the tactical and strategic parts of your job with ease.
- [ ] During the first 2-4 weeks, focus on:
     1. Getting through your general [GitLab Onboarding Issue](`<INSERT actual issue link>`) 
     1. Making sure you understand this issue and connect with your manager to start shaping the [Strategy](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/PM-onboarding-first-100-days.md/#strategic) focused goals you'll focus on during your first 3 milestones at GitLab
- [ ] Within the first 6 weeks, try to get through the [Tactical](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/PM-onboarding-first-100-days.md/#tactical) focused tasks below

## Weeks 4 and beyond (your "first 100 days" at GitLab)
# Tactical 
_Work through these tasks in your first 6 weeks on the job. These items will help you understand how to perform and succeed in your [Product Manager Role](https://about.gitlab.com/handbook/product/product-manager-role/) at GitLab._

#### Product Process
* [ ] Read and familiarize yourself with the [main Product Handbook pages](https://about.gitlab.com/handbook/product/)
  * [ ] [Product Manager Responsibilities](https://about.gitlab.com/handbook/product/product-manager-responsibilities/)
  * [ ] [Product Processes](https://about.gitlab.com/handbook/product/product-processes)
  * [ ] [Product Principles](https://about.gitlab.com/handbook/product/product-principles/)
  * [ ] [Being a product manager at GitLab](https://about.gitlab.com/handbook/product/product-manager-role/)
  * [ ] [Product OKRs timeline/process](https://about.gitlab.com/handbook/product/product-okrs/)
  * [ ] Familiarize yourself with [GitLab's Fiscal Year](https://about.gitlab.com/handbook/finance/#fiscal-year) to understand the abbreviations (FY22-Q4, FY23-Q1) you're likely to see and hear in Product discussions.
  * [ ] Familiarize yourself with [product categories]( https://about.gitlab.com/handbook/product/categories/#devops-stages) to understand how Gitlab teams are organized
  * [ ] Familiarize yourself with the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). It's important to know where we are in the cadence and start to work ahead as quickly as possible.
  * [ ] Familiarize yourself with [product development flow](https://about.gitlab.com/handbook/product-development-flow/index.html). Pay attention to how labels, such as `workflow::xxxxx`, `direction`, `deliverable`, and `planning priority` work.
* [ ] Review [product operations in the product handbook](https://about.gitlab.com/handbook/product/product-operations/) 
* [ ] Ask questions in the #product-operations or #product Slack channels to get clarification on things you are uncertain about
* [ ] Review [the UX Department page in the Product handbook](https://about.gitlab.com/handbook/product/ux/) and set up a coffee chat with with VP of UX `@clenneville`
* [ ] Ask questions in the #UX or #product Slack channels to get clarification on things you are uncertain about


#### Routine PM Activities
* [ ] Have a coffee chat with each of the other PMs in your Section
* [ ] Review the boards that your development group uses for planning, scheduling, building, or other activities. If you don't know where they are, ask your EM!
    - [ ] Organize your boards, issues, and epics. It is likely things have gotten unorganized over time
    - [ ] Organize your group issues into [Epics](https://about.gitlab.com/handbook/product/#meta-epics-for-longer-term-items) including [Category Maturity Plan Epics](https://about.gitlab.com/handbook/product/#maturity-plans)
    - [ ] Don't hesitate to [close issues as needed](https://about.gitlab.com/handbook/product/#when-to-close-an-issue)
* [ ] Familiarize yourself with [Triage issues](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#triaging-issues), which are [created for groups automatically](https://gitlab.com/gitlab-org/quality/triage-reports/-/issues). Update [group-definition.yml](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/group-definition.yml) to assign these issues to yourself for your group.
* [ ] Add yourself to the [PM Team Monthly Kickoff template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Monthly-Kickoff.md) to ensure you get correctly tagged on your product section
* [ ] Review [recent release posts](https://about.gitlab.com/blog/categories/releases/), in particular the main monthly releases, for what's been changing in your stage.
* [ ] Coordinate with your engineering manager on what will be delivered in the current release; help ensure [release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/) content is created and merged at the appropriate time in time. If you need help with the release post ask questions in Slack #release-posts or ask a fellow PM, your manager, or your EM. 
     - [ ] Add your name/start date to the release post [tracker ](https://docs.google.com/spreadsheets/d/12tFW2nOqZ7Cxm0T-WKZVHmPdZNPtkS6fdIvLwvWVLLc/edit#gid=0)and create a reminder for yourself to sign up to [lead a release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/#volunteering-for-the-release-post) sometime after your first 3 months at GitLab
* [ ] Review [the direction page](https://about.gitlab.com/direction), in particular the sub-page for your stage and the direction pages for the categories you manage. You can find links directly to your category vision pages on the categories handbook page. You will be responsible for updating and maintaining your direction page(s) with what your group is working on and why. 


#### Learning Product Management at GitLab
* [ ] Explore the [Product Management Learning and Development handbook page](https://about.gitlab.com/handbook/product/product-manager-role/learning-and-development/) and corresponding [Product Team Learning Hub in GitLab Learn](https://gitlab.edcast.com/channel/gitlab-product-team-learning-hub). All content in GitLab learn must be in the handbook but some handbook content isn't yet in GitLab learn, so check out both! 
  * [ ] Complete the [GitLab Level Up training on Iteration](https://levelup.gitlab.com/courses/product-iteration)
  * [ ] Assign interesting topics/content to yourself in the Learning Hub.


#### User Research and Design
1. [ ] Familiarize yourself with our customer [Roles and Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)
1. [ ] Familiarize yourself with the capabilities and process of our [UX Research Team](https://about.gitlab.com/handbook/engineering/ux/ux-research/) including their [UX Insights issue repository](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=closed).
1. [ ] Familiarize yourself with [GitLab's Design System](http://design.gitlab.com).
1. [ ] Read about the [UX Research shadowing process](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-shadowing). You can kick off the process when you meet with the UX Researcher for your group. Product Managers should go through research shadowing before executing UX research on their own.
1. [ ] Review recent analyst research in the #analyst-relations slack channels relevant to your stage and category.
   - [ ] Setup a meeting with Analyst Relations Manager Ryan Ragozzine (`@rragozzine`) to introduce yourself, receive relevant research, and begin getting involved in analyst activities in your area.


#### Engage with Customers
* [ ] Figure out who your internal customers are. Set up regular check-ins in with your Internal Customers (also found on the categories handbook page); try to aim for a monthly cadence. 
* [ ] Reach out to users via popular issues and schedule user interviews to better understand the problems our users face


#### Interview PM Candidates
1. [ ] Complete Interview Training
   - [ ] Ping #recruiting-product and open an issue for interview training. Add a due date to complete this issue no later than the end of your 3rd month at GitLab.


#### Introduction to Data
1. [ ] Consider joining the #data channel on Slack, as it's the best place to reach out to the data team.
1. [ ] Consider joining the #data-lounge channel on Slack, as it's where data-related resources get shared.
1. [ ] The data team project can be found at gitlab.com/gitlab-data/analytics. If you have data requests, be sure to create issues there.
1. [ ] Review the [Data for Product Managers](https://about.gitlab.com/handbook/business-technology/data-team/programs/data-for-product-managers/) page in the handbook.
1. [ ] There is a lot more information on data at GitLab in the [Data Team Handbook](https://about.gitlab.com/handbook/business-technology/data-team/#data-team-handbook)
1. [ ] Watch ["The State of Product Data"](https://www.youtube.com/watch?v=eNLkj3Ho2bk&feature=youtu.be) from Eli Kastelein at the Growth Fastboot. (Be sure you have accepted your invitation to be a Manager of GitLab Unfiltered during Day 4, then go to the profile menu in the upper right and select "Switch account" => "GitLab Unfiltered").


##### Sisense (formerly Periscope)
Sisense (formerly Periscope) is GitLab's data analysis and business intelligence tool. Confirm you can access. This should have been part of your [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_product_management/role_product_manager.md).
1. [ ] Obtain access to [Sisense](https://about.gitlab.com/handbook/business-technology/data-team/platform/periscope/), our data analysis tool.
   1. [ ] Review the [getting started with Sisense guide](https://doc.periscopedata.com/article/getting-started)
   1. [ ] Familiarize yourself with relevant [dashboards](https://app.periscopedata.com/app/gitlab/topic/Product/ab707846c91f4d30b1c1ca0399803d67).


##### Pings (Product)
This data comes from the service ping that comes with a GitLab installation.
1. [ ] Read about the [service ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html).
1. [ ] Read the product vision for [Product Intelligence](https://about.gitlab.com/direction/product-intelligence/).
1. [ ] Read the documentation on [service ping](https://docs.gitlab.com/ee/development/service_ping/)
1. [ ] It might be helpful to look at issues related to [Product Intelligence](https://gitlab.com/gitlab-org/product-intelligence/-/issues).

##### Snowplow (Product)
[Snowplow](https://snowplowanalytics.com) is an open source web analytics collector.
1. [ ] To understand how this is implemented at GitLab, read [GitLab Implementation](https://about.gitlab.com/handbook/business-technology/data-team/platform/snowplow/#gitlab-implementation).
1. [ ] Also read how we pull data from [S3 into Snowflake](https://about.gitlab.com/handbook/business-technology/data-team/platform/snowplow/#s3)
1. [ ] Familiarize yourself with the [Snowplow Open Source documentation](https://github.com/snowplow/snowplow).
1. [ ] We use the [Snowplow dbt package](https://hub.getdbt.com/fishtown-analytics/snowplow/latest/) on our models. Their documentation does show up in our dbt docs.


##### Metrics and Methods
1. [ ] Read through [SaaS Metrics 2.0](http://www.forentrepreneurs.com/saas-metrics-2/) to get a good understanding of general SaaS metrics.


# Strategic 
_Work on the items below during your first _full_ 3 milestones at GitLab. Depending on your start date, your third milestone may fall past 100 days, which is ok._

### Define specific goals you would like to accomplish in `<Release 1>`, `<Release 2>`, `<Release 3>`
Below is some recommendation and guidance on what to focus during your first few months on the job. It's important to identify value-driven goals in partnership with your manager. This will help you prioritize the right things for your success as well as making a meaningful impact during your ramp up at GitLab. When devising these goals, keep in mind it will set the baseline and serve as a guide for discussion with your manager in your first [CDF](https://about.gitlab.com/handbook/product/product-manager-role/#product-manager-onboarding) review. Make make sure you understand the goals below and request revise them with your manager if you want to define different goals. 

## First Full Milestone `<Release 1>`

### Actively participate in all <group> activities
* Be responsible for the planning issue in the next milestone
* Be responsible for release post items for current milestone
* Update your [category direction pages](https://about.gitlab.com/direction)
* Actively ask for help
* Actively plan for problem validation or solution validation
* Sign up for the [Continuous Interviewing Course](https://learn.producttalk.org/p/continuous-interviewing)

#### Helpful Links

* [Walkthrough of creating release post items](https://youtu.be/q6veGiHauRs)
* [Instructions for release post items](https://about.gitlab.com/handbook/marketing/blog/release-posts/#pm-contributors)
* [Walkthrough of updating a direction page](https://studio.youtube.com/video/3RTgjx6diGs/edit)
* [Walkthrough of the monthly PI updates](https://youtu.be/AW12hmEunZw)

### Understand Strengths and Weaknesses of your group's product
It is imperative to understand not only the fantastic new features of your group's product, but to also have a good understanding of the weakness and problems. Some suggested ways to achieve this understanding are:

* [ ] Talk to your support stable counterparts
* [ ] Schedule customer interviews with both existing customers, and new customers and pay attention to what needs are being met, what needs are not being met, and what barriers there are to entry.

## Second Milestone `<Release 2>`

### Own the categories activities
* You've done the entire process from start to finish once, do it once more with more ownership
* It's ok to not know everything. 
* Ask for help often, it is a good practice no matter how experienced you are
* Update your category direction pages post release

### Understand how your group's product stacks up to the competition
No matter how great our product is, there are strong competitors in the marketplace, and we should never discount their influence and innovation. In that regard, we should strive to understand what makes our competition better than us, and how we can best prioritize features to ensure we strategically compete. Some opportunities for this include:

* [ ] Talk with analyst relations and product marketing stable counterparts
* [ ] Create walkthroughs of competitive products and highlight our strengths and weaknesses observed through this exercise
* [ ] Build deep relationships with our internal teams so we can effectively facilitate the Dogfooding of our product
* [ ] Work on discovery skills - Opportunity canvas lite for a chosen topic

## Third Milestone `<Release 3>`

### Feel comfortable running the day-to-day activities as the PM for your category

### Understand how your group's product fits within GitLab

It's easy to focus entirely on your own group when starting your PM journey at GitLab. However, one of our main competitive advantages is that we have a single application. Because of this, as a GitLab PM, we need to have a good understanding of adjacent pieces of our product in order to ensure we complement one another.

* [ ] Collaborate with adjacent stage PMs
* [ ] Spend time actually using different portions of the product to better understand where your group's contributions may fit in for additional cross-stage collaboration.

### Discovery

One of the most important activities for a PM is understanding the user or customer's problems and pain points. Get familiar with GitLab's [validation track](https://about.gitlab.com/handbook/product-development-flow/#validation-track).

* [ ] Dig deeper into a feature under your responsibility - create and present an [opportunity canvas lite](https://about.gitlab.com/handbook/product/product-processes/#opportunity-canvas-lite) on it.

### Everyone can contribute!

Have you found any mistakes in this guide or have ideas to improve, please contribute to the [first 100 days template](https://gitlab.com/-/ide/project/gitlab-com/Product/edit/main/-/.gitlab/issue_templates/PM-onboarding-first-100-days.md#).

/label ~onboarding Doing
