_This template should be personalized by each Section for their [monthly Section PI reviews](https://about.gitlab.com/handbook/product/product-processes/#section-performance-indicator-review-monthly). Please note any items that are required are noted as **REQUIRED**._

## :book: References

* `[Handbook Page](ADD LINK)`
* `[Meeting Notes](ADD LINK)`
* `[PI .yml file](ADD LINK)`

## :dart: Intent
We are organizing for and regularly reviewing Performance Indicators (PIs) in order to enable a dialog between ourselves (all of R&D) about most effective use of our R&D efforts towards the most impactful improvements.

As a communication tool, Performance Indicators are only as useful as the range of the audience. As a result we should:

* store PI metrics, funnels, growth models, status and next steps handbook first (avoid content only living in slides)
* expose our product groups to PI metrics regularly
* communicate transparently about adjustments to PIs within our groups
* reference PIs in planning issues and prioritization discussions

**REQUIRED** - In each meeting, we will cover the items/structure below that maps to the PI pages in the internal handbook:

* Section MAU
* Stage MAU
* Stage Error Budget
* Stage Availability
* Stage Security Backlog (Past Due S1/S2*)
* Stage UX Backlog (SUS S1/S2)
* Stage Bug Backlog (Past Due S1/S2)
* Stage XFP (Issue and MR Ratios)
* Group MAU
* Group Error Budget
* Group Availability

**OPTIONAL/READ ONLY** - In each meeting, we may cover these items as needed that map to the PI pages in the internal handbook:

* Group Security Backlog (Past Due S1/S2*) 
* Group UX Backlog (SUS S1/S2) 
* Group Bug Backlog (Past Due S1/S2) 
* Group XFP (Issue and MR Ratios) 

### PM responsibility

- Present monthly data about:
     - Group's focus
     - Analysis of monthly user activity against quarterly target(s)
     - Lessons Learned

### EM Responsibility

- Review and report on:
     - Error Budgets
     - Security Burndown
     - Reliability

### UX Responsibility

- Review and report on:
     - S1/S2 SUS impacting burndown

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Set a due date on this issue as the last business day of the week prior to the scheduled review - `@section-leader`
* [ ] Add a restrospective thread to this issue - `@section-leader`
* [ ] Add stub MR for PI updates - `@section-leader`
* [ ] Prep meeting agenda - `@section-leader`

### :star: Performance Indicator YAML Updates

Each product stage or group should update the `[Performance Indicators YAML file](ADD LINK)` with:

* Update your `Health:` and `Instrumentation:` levels
* Update any `URLs:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update any `URLs:` to reference additional Sisense dashboards for more details
* Update `Implementation Status:` with the current maturity
* Update `Lessons Learned:` with your takeaways from the previous month's data
* Update `Focus This Month:` with your plan for the month ahead 

### :star: Expectations
1. Ensure your graph is refreshed
1. Ensure your graph looks correct and if it's not, understand why and change it
1. Have a comprehensive understanding of your graphs (understand history, challenges with the data, etc) and document directly in the .yml file
1. Ensure there are no graph labeling or title errors
1. Ensure you have a target set and you know how that target was set. If you can't have a target, in the target field, please explain why you don't have a target and when you would expect to have one.

### :star: Trend Review
1. Review [Prioritization dashboard](https://app.periscopedata.com/app/gitlab/1042933/Issue-Types-by-Milestone) & [MR type dashboard](https://app.periscopedata.com/app/gitlab/976854/Merge-Request-Types-Detail) filtered for team_group on:

- To be added by Section Leader

1. Answer questions in monthly meeting agenda regarding dashboard data accuracy, error budgets, and prioritization trends
#### Group Updates

- To be added by Section Leader

### :x: Closing Tasks
* [ ] Make `[adjustments to the template](ADD LINK)` based on the retrospective thread - `@section-leader`
* [ ] Activate on any retrospective thread items - `@section-leader`

/assign `@section-leader`
`/label - add approprate Section labels`
