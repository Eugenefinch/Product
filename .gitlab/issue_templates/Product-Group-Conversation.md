## Overview

This issue serves as a reminder and checklist for the upcoming [Product Group Conversation](https://about.gitlab.com/handbook/product/product-leadership/#product-group-conversation-leader-rotation). 

### Tasks for @justinfarris:

- [ ] Set the due date of this issue to be the date of the next Product Group Conversation

- [ ] Add a link to this issue for reference to the rotation schedule [here](https://about.gitlab.com/handbook/product/product-leadership/#product-group-conversation-leader-rotation)
- [ ] Assign this issue to the assigned PLT host pulled from this rotation eschedule [here](https://about.gitlab.com/handbook/product/product-leadership/#product-group-conversation-leader-rotation)

### Tasks for the PLT Host: 

#### :o: Opening Tasks

- [ ] Make sure your name is listed in the PLT rotation and you've read the overview content [here](https://about.gitlab.com/handbook/product/product-leadership/#product-group-conversation-leader-rotation) 
- [ ] Prepare for the Group Conversation by reviewing [Group Conversation](https://about.gitlab.com/handbook/group-conversations/) guidance, paying special attention to [addtional details for functional presentations](https://about.gitlab.com/handbook/group-conversations/#additional-consideration-for-functional-presentations)
- [ ] Look at the latest Product Key Review deck [here](https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg) for reference to prepare your presentation. If there's are Key slides that are more recent than the last Group Conversation, you can leverage those for your presentation
     - If the lastest Key slides are too outdated, you can find and leverage the most recent Group Conversation slides [here](https://drive.google.com/drive/folders/13E3QBvw8ojeb24Zu1bfCETUNsRTH35ia) for your presentation. 
     - Note: If you're not using a most recent Key slides presentation, you'll need to request any necessary updates from the team by tagging them into the slides directly.
- [ ] If you add any content to the presentation, it is recommended you share your deck as a heads up to the VP, Product 3 calendar days prior to the scheduled Group Conversation in Slack #product-plt
- [ ] Add a link to your Group Conversation presentation to this issue and [Gdrive](https://drive.google.com/drive/folders/13E3QBvw8ojeb24Zu1bfCETUNsRTH35ia), regardless of whether it has modifications from any  original leveraged Product Key slides:
     - Date: XXXX-XX-XX
     - Link: 

#### :x: Closing Tasks

- [ ] Check  [PLT rotation ](https://about.gitlab.com/handbook/product/product-leadership/#product-group-conversation-leader-rotation). If the upcoming Group Conversation is blank, post a comment in this issue tagging `@gl-product-plt ` asking for volunteers 
- [ ] Add a restrospective thread to this issue
- [ ] Activate on any retrospective thread items including updating this [updating this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/product-gc-host-rotation.md)
- [ ] Close this issue
    
/label ~"Product Operations"
/confidential
/assign @jennifergarcia20 @@gschwam @david @justinfarris
