## Introduction 

Two sub-values at GitLab are [sharing](https://about.gitlab.com/handbook/values/#share) and [findability](https://about.gitlab.com/handbook/values/#findability). In the Product Management organization, we strive to transparently communicate where we are headed and celebrate recent achievement in our Product Direction. The [Product Direction Showcase](https://about.gitlab.com/handbook/product/product-processes/#product-direction-showcases) is a monthly meeting featuring 4 stage groups. 

This month each presenting group has 10 minutes per stage split between the Direction overview and Deep Dive. There are 2 minutes of Q&A between stage.

## Opening Issue Tasks 

- [ ] Update title with month - @jreporter 
- [ ] Add retrospective thread - @jreporter 
- [ ] Confirm assignees are stage leaders - @jreporter
- [ ] Set due date for 1 week before Product Direction Showcase - @jreporter 
- [ ] Confirm meeting is scheduled with Zoom  - @jreporter 
- [ ] Update [Agenda](https://docs.google.com/document/d/13BuRLiDJpNNhGmO1dOPqMQR0yaEDZY4CFGFR49tZMYo/edit?usp=sharing)  - @jreporter 

## Participant/Speaker Actions to take 

- [ ] Add your stage to a table slot below 
- [ ] Add deep dive topic for stage and tag relevant Product Manager 
- [ ] Add links to direction page or deck 48 hours before scheduled session 
- [ ] Request async review of materials 24 hours before meeting - @jreporter 

### Stage order table 

| Order | Stage | Direction DRI | Deep Dive DRI  | Deep Dive Topic  | Links  |
| --- | --- | --- | --- | --- | --- |
| 1 |  | Incubation Engineering  |  |  |  |
| 2 |  |  |  |  |  |
| 3 |  |  |  |  |  |
| 4 |  |  |  |  |  |

## Closing Issue Tasks 

- [ ] Resolve any retrospective items by updating i[ssue template ](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/product_direction_showcase.md) - @jreporter 
- [ ] Identify next month's presenters in a comment thread "Next Month Presenter Thread" - @jreporter 
- [ ] Share recording in #product, #whats-happening-at-gitlab  - @jreporter 
- [ ] Add Livestream to [Product Direction Showcase YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr2okKqlDUZ_zz_Y-LW7zoW) - @jreporter 
- [ ] Close Issue  - @jreporter 

/assign @jreporter @sarahwaldner @fzimmer @mushakov @hbenson @jennifergarcia20 @bmarnane @tmccaslin
/due in 14 days 
