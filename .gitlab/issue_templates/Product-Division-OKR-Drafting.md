### Overview

The Chief Product Officer uses this issue to propose Product Division OKRs, which align to our three [FY24 Product Investment Themes](https://about.gitlab.com/direction/#fy24-product-investment-themes) and the CEO OKRs as needed.  Please see the Product Division OKR workflow and timeline [here](https://about.gitlab.com/handbook/product/product-okrs/). 

### To do
- [ ] `@justinfarris` Update issue title, due dates and feedback issue link in this issue and tag CProdO to activate
- [ ] `@justinfarris` 2 weeks prior to new quarter start, assign to EBA to create Product Division OKRs in GitLab and tag all `@gl-product` for review and feebdack in GitLab OKRs directly.

### Workflow

Product Operations facilitiates the workflow below by tagging team members in to review/action as needed.

1. CProdO tags in all Product Leaders `@gl-product-plt` to review, confirm viability or propose alternates for review and discussion if needed. 
2. Once PLT dicussion is complete, EBA to create Product Division OKRs in GitLab and tag all `@gl-product` for review and discussion in GitLab OKRs directly. This should happen at least 2 weeks prior to quarter start. 
    - ask product leaders to ensure their section/stage/groups stay on track with overall timeline to review and finalize OKRs by start of the new quarter. 
    - ask all product leaders to collbaorate with their respective quads to draft and commit to their KRs for the quarter 
3. Share out finalized list in GitLab OKRs at the start of the new quarter.
4. Everyone please contribute to the `[FYXX QX feedback issue](ADD LINK)`

### Proposed OKRs

Initial drafts be added by the Chief Product Officer

The following are the current proposed OKRs for the Product Division. Note, we will align closer across R&D where items overlap with the [Quad](https://about.gitlab.com/handbook/engineering/quality/quality-engineering/quad-planning/).

#### **OKR1: Product investment theme 1

##### KR1: 
- Description:
- Metric: 
- owner: 

##### KR2: 
- Description:
- Metric: 
- owner: 

#### OKR2: Product investment theme 2

##### KR1: 
- Description:
- Metric: 
- owner: 

##### KR2: 
- Description:
- Metric: 
- owner: 

#### OKR3:Product investment theme 3

##### KR1: 
- Description:
- Metric: 
- owner: 

##### KR2: 
- Description:
- Metric: 
- owner: 

#### OKR4: CEO OKR

##### KR1: 
- Description:
- Metric: 
- owner: 

##### KR2: 
- Description:
- Metric: 
- owner: 


## Independent Section/Team level OKRs 

Product Division leaders may create OKRs not related to the Product Team Objectives above. However, please be mindful of creating prioritization/capacity conflicts, especially if x-functional effort is needed to complete the work. Please us the suggested format below that refer to either the Section or departments as appropriate. 

#### Section: Sec

#### Section: Core Platform

#### Department: UX

#### Sub-department Technical Writing

/confidential
/label ~"Product Operations' ~"workflow::indev" ~"OKR" 
/assign @david @justinfarris @jennifergarcia20 @gschwam
