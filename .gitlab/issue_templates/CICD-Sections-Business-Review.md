## :book: References

* [Meeting Notes](https://docs.google.com/document/d/1n_b3L2_OYEnyYC03rBbha_DQfzLasU5I-koBz2SwUwI/edit?usp=sharing)

## Objective 

This issue is to help organize a cross-section review of top priorities which are as follows: 

  * OKR Progress
  * Error Budgets
  * SLO/SLA of Bugs for each product group and stage

There is a sync for CI/CD sections leaders and teammates to join and disucss blockers, obstacles, and celebrate. The DRI for the CICD Business Review is Mike Flouton (@mflouton). 

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Set a due date to this issue as 2 business days prior to the scheduled review to make time for Leadership async review -  @jreporter 
* [ ] Add a retrospective thread to this issue -  @jreporter 
* [ ] Update the [Meeting Notes](https://docs.google.com/document/d/1n_b3L2_OYEnyYC03rBbha_DQfzLasU5I-koBz2SwUwI/edit?usp=sharing) with the timing and this Prep Issue - @jreporter 
* [ ] Check remaining follow ups from last reviews -  @jreporter 
* [ ] Coordinate timing of reviews from Directors and Product Design Manager `@mflouton @sgoldstein @jreporter @rayana @vincywilson`-  @jreporter 

### Error Budget and SLA/SLO updates

- [ ] on Error Budget, Security, Infradev - @sgoldstein - ([add any MR updates](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/))
- [ ] on OCBA S1/S2, Overall Bug hygiene - @vincywilson
- [ ] on SUS - @rayana 

### OKR updates 

- [ ] Verify:Pipeline Execution OKR Progress %, Health, and Blockers - @jreporter, @carolinesimpson
- [ ] Verify:Pipeline Authoring OKR Progress %, Health, and Blockers - @dhershkovitch, @marknuzzo
- [ ] Verify:Pipeline Security OKR Progress %, Health, and Blockers - @jocelynjane, @shampton
- [ ] Verify:Runner SaaS OKR Progress %, Health, and Blockers - @gabrielengel_gl, @nicolewilliams
- [ ] Verify:Runner Core & Fleet OKR Progress %, Health, and Blockers - @DarrenEastman, @nicolewilliams
- [ ] Package OKR Progress %, Health, and Blockers - @trizzi, @crystalpoole
- [ ] Deploy OKR Progress %, Health, and Blockers - @nagyv-gitlab, @nmezzopera
- [ ] Quality OKR Progress %, Health, and Blockers - @vincywilson 
- [ ] Product Design OKR Progress %, Health, and Blockers - @rayana, @mvanremmerden

### :package: Pre-Meeting Tasks
* [ ] Consider pre-recording an overview - @jreporter 
* [ ] Request Section Leader review - @jreporter 
   * [ ] `@mflouton`
   * [ ] `@sgoldstein` 
* [ ] Ensure ample time for Product Manager and Engineering Manager review of all stages - @jreporter 
* [ ] Ask questions in the [doc](https://docs.google.com/document/d/1n_b3L2_OYEnyYC03rBbha_DQfzLasU5I-koBz2SwUwI/edit?usp=sharing) 
    * [ ] @jreporter 
    * [ ] @vincywilson
    * [ ] @sgoldstein 
    * [ ] @mflouton 
    * [ ] @rayana  
    * [ ] @cheryl.li
* [ ] Pay special attention to progress towards annual goals for CMAU and SMAU - @jreporter 
* [ ] Share the updated PI handbook page in the `#ci-section` `#cd-section` and `#product` Slack channels 2 business days in advance of the review and encourage async participation - @jreporter 

### :night_with_stars: Post-Meeting Tasks
* [ ] Capture actions discussed in the review as comments in issue - @jreporter 

### :x: Closing Tasks
* [ ] Activate on any retrospective thread items - @jreporter 
* [ ] Add a reminder to team members to capture their follow-ups in issues and add them as an issue comment - @jreporter 
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/CICD-Sections-Business-Review.md) based on the retrospective thread - @jreporter 
* [ ] Add a highlights comment and share in Slack - @jreporter 

/assign @dhershkovitch @DarrenEastman @trizzi @jreporter @nagyv-gitlab @jocelynjane @hbenson @sgoldstein @mflouton @rayana @gabrielengel_gl 
/label ~"Business Review Prep"