# Overview

The <%= FiscalDateHelper.new(Date.today).this_quarter %> iteration of Gitlab SaaS PNPS will be a series of email surveys, deployed as needed, with the intention of: 

1. Delivering an overall score by end of the quarter to track against our previous quarter score
1. Deliver scores by plan type, ensuring we get a sample that's representative of plan type breakdown
1. Backtrack and deliver a breakdown of Detractors plan types if needed
1. Maintaining response rate of ~3%
1. Reveal PNPS data into Sisense for viewing/tracking
  
**Requirements & Methodology:**

*  Paid users gitlab.com
     * Can include admins (payers) or users of the product
     * Can include users with free access to paid features (such as educational institutions with Gold for free)
*  Survey eligible users with the goal of achieving representative breakdown by plan type within +/- 3% accuracy compared to [the SaaS plan proportion source of truth](https://app.periscopedata.com/app/gitlab:safe-dashboard/919360/TD:-Licensed-Users-by-Product-&-Rate-Plan-Name)
*  Minimum 60 day subscription
*  Aim for 95% confidence interval
*  No user should get a survey more than once a year

# Survey Questions:

We are using the standard Net Promoter Score questions:

1. How likely are you to recommend GitLab to a friend or colleague? (Scale 0 to 10 where 0 = Not at all likely and 10 = Very likely)
Why? _(Open ended text field)_

1. Would you be willing to talk further with someone at GitLab about your experience? This would be for research purposes, not for sales or marketing. (If yes) Please enter the email address where you would like to be contacted. _(Open ended text field)_

# Tools & Process: 

via Qualtrics

1. Pull a list of users for the quarter using [this query](https://app.periscopedata.com/app/gitlab:safe-dashboard/919244/Growth-UXR-Scratch?widget=12667049&udv=0). Those users are SaaS users on a paid plan (though that includes people getting a paid plan for free, like OSS/EDU).
1. Check the list against the [list of emails sent in the previous quarters for the year](https://docs.google.com/spreadsheets/d/1Q9xOh5L9QJGdW9tq89rcADvo5jozRxdo7KiO2z0HLAU/edit#gid=0), scrubbing anyone previously contacted.
     - This can be done by applying this formula `=VLOOKUP([cell reference],'Previous contacts'!A:A,1,False)` in a new tab of that sheet.
1. [Look up the overall SaaS percentage breakdown by plan.](https://app.periscopedata.com/app/gitlab:safe-dashboard/919360/TD:-Licensed-Users-by-Product-&-Rate-Plan-Name) Our end goal will be to achieve a sample breakdown that roughly matches our population breakdown (+/-3%)
1. Send out waves of 5,000-10,000 emails each using the email template in Qualtrics called `PNPS Survey Distribution - Current Survey Template`. This email template is in the UXR Qualtrics account in the `Organization Library: UX Research & Product`
     - Remove user's email addresses that are listed as opt-outs. For details on the process, see the following [handbook section](https://about.gitlab.com/handbook/product/ux/qualtrics/#distributing-your-survey-to-gitlabcom-users).
1. After each wave:
     - Download the list of completed responses and select `Use choice text`. Open the .csv then copy and paste the contents into a new `wave #` tab of the user list sheet.
     - Look at the current plan breakdown in the `Percentages` tab of the sheet.
     - Adjust the next wave to compensate for the current response breakdown, adding or subtracting the quantities of a plan for that wave as needed. 
1. Send out additional waves until we reach approximately 500 completed responses
     - You may to repeat steps 1-3 until the goal of at least 500 completes is met
1. Once we've reached our desired sample size, download a .csv file from Qualtrics then copy and paste the contents to the Analysis in a Google Sheet.


[SaaS plan proportion source of truth](https://app.periscopedata.com/app/gitlab:safe-dashboard/919360/TD:-Licensed-Users-by-Product-&-Rate-Plan-Name)

`[QX FYXX user list used](ADD LINK)` `@wleidheiser` 

`[QX FYXX Raw analysis CSV Google sheet](ADD LINK)` `@wleidheiser` 

`[QX FYXX all written responses list](ADD LINK)` `@wleidheiser`

# Estimated Research Timeline

- Data collection: 4-6 weeks  
   - 2 hours of initial preparation work
   - 1 hour per week distributing each wave
- Data analysis: 1 week
   - 8 - 10 hours of cleaning data, categorizing verbatims, creating graphs and writing report
- Editing report draft: 1-2 weeks
   - 1 hour of sync review with stakeholders
   - 1 hour of responding to stakeholder feedback and updating report

# Next Steps 

- [ ] Add due date to issue 
- [ ] Add milestone to issue 
- [ ] Add issue to appropriate epic `FYXX PNPS Surveys`
- [ ] Add due dates for the task lists below 
- [ ] Update links to user list used, all written and raw data/analysis sheets in this issue description 
     - Please create and maintain all PNPS files [in this shared folder](https://drive.google.com/drive/folders/1KT5qpJVWFWY_3eVqQzK8tw7ktEKhyDPn) 
- [ ] Contact `@caitlin` to make her aware before sending out surveys to users
- [ ] Launch surveys 
- [ ] Add data from `PNPS Analysis` sheet to `PNPS Open Responses` and `PNPS Followup Users` sheets
- [ ] **(Submit issue 2 weeks before data is needed)** Provide UXR with responders' previous plan types `@dpeterson1` (only upon request by creating a Product Data Insights issue; see past [issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/1006) as an example)
- [ ] First analysis and draft report for UXR peer review 
- [ ] First draft report for UXR peer review
- [ ] Final version deck complete 
- [ ] Share out for feedback across teams: Slack #product, #UX, #Development, #Quality #Sales, #Customer-success, #Marketing #product-marketing, #ux_research_reports
- [ ] Add analysis slides to [prod ops survey page](https://about.gitlab.com/handbook/product/product-operations/surveys/) 
- [ ] Add link to analysis slides to read-only section of PM biweekly meeting 
- [ ] Update PNPS Responder issue and follow steps in the issue
- [ ] Update [NPS previous contact list sheet](https://docs.google.com/spreadsheets/d/1Q9xOh5L9QJGdW9tq89rcADvo5jozRxdo7KiO2z0HLAU/edit#gid=0)
- [ ] Update [issue template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/ProdOps-PNPS-Collection-Analysis.md) as needed with any changes or learnings from the quarter

/confidential

/assign @wleidheiser
