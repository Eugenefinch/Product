<!-- Use this issue for tracking team members who help out with other group work. This isn't a formal Borrow Request where multiple team members need to work on something else for a specific period of time, or multiple projects change in priority. Use this when team members pick up an issue or two in another group, or works on something else for a few milestones because they have extra bandwidth.-->

## Borrow Request Overview

<!-- Outline the goals and reasoning supporting the Borrow Request, including rough time frame and scope of effort (how many days, weeks, or milestones -->

### Exit Criteria

<!-- Define the exit criteria expected from the Borrow Request -->

### Proposal

<!-- Define the ask:
  1. Who is the team member
  2. What work are they picking up, and for how long
  4. Define DRI
-->

<!-- Example
1. **Fulfillment: Provision Product Designer will pick up two issues for Release in 14.9
1. Timeline: 25% of bandwidth in 14.9
1. The DRI is the Product Design Manager for Fulfillment
-->

### Issue Links

<!-- Link to relevant issue/s -->

### Onboarding Tasks

<!-- List tasks for team members to get aquainted with the project and/or team -->


## Process Overview

- [ ] Create issue outlining work change and reasoning
- [ ] Share issue with impacted Group Product Managers, Engineering Managers and Product Design Managers
- [ ] Complete any scoping of the work to be done
- [ ] Request review and approval from your manager
- [ ] Identify any onboarding tasks needed for success
- [ ] Incorporate feedback into the issue description
- [ ] Execute
- [ ] Retrospective - Create an issue (optional), or use an existing Retrospective issue (for example, if a Product Designer works with another Section for one milestone, they could share feedback on the experience in the UX Retrospective.)

/confidential
/label ~"borrow::requested" 
