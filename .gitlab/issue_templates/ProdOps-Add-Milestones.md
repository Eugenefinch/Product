## 🎯 Intent

Make sure the milestones have been added to the `gitlab-org` and `gitlab-com` groups for the upcoming quarter.

Follow [this guidance in the handbook](https://about.gitlab.com/handbook/product/product-processes/#product-milestone-creation) to update:

- [ ] .org
- [ ] .com

When the upcoming quarter of milestones are added:

- [ ] Close this issue

/assign @brhea
/assign @fseifoddini
