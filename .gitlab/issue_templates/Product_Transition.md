### Overview 

This issue will serve as a tracker for the transition from `Current role` with `Current stage/group` to `New role` with `New stage/group`. 

### Transition Overview 

* Effective Date: YYYY-MM-DD
* Announcement Date: YYYY-MM-DD
* `Current role` Duties: 
   * Assignee: Tasks
   * Assignee: Tasks
   * Assignee: Tasks

### Opening Tasks 

* [ ] Create transition issue 
* [ ] Share issue with Manager 
* [ ] Once announcement is final move to Product Project 
* [ ] Add retrospective thread for updates to template
* [ ] Share relevant documents with parties 

### Timeline 

#### Week 1

* [ ] Update direction pages 
* [ ] Share issue in your impacted channels 
* [ ] Collect documents and add to `Relevant Documents` section 
* [ ] Add Task
* [ ] Add Task

#### Week 2

* [ ] Schedule syncs with Transition Parties (if needed) 
* [ ] Transition any documents, open problem/solution validation 
* [ ] Transition open release post MRs
* [ ] Transition any customer calls scheduled to new PM 
* [ ] Share planning issues
* [ ] Bulk update issues 
* [ ] Update reporting structure in Bamboo 

### Updates 

* [ ] Update [team yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) files to reflect changes in role, managers, and specialty if needed
* [ ] Update [stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml)
* [ ] Product [Issue Templates](https://gitlab.com/gitlab-com/Product/-/tree/main/.gitlab/issue_templates)
   * [ ] [Monthly Kick Off](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Monthly-Kickoff.md)product-development-retro.md)
   * [ ] Stage specific Performance Indicator issue templates
* [ ] Update triage report assignment in this [policy](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/group-definition.yml)
* [ ] Update Team/Group/Stage Project permissions 
* [ ] Update Team Retrospective Assignments in this [repo](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml) and [Organization Retrospective](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/)
* [ ] Update any www-gitlab-com [CODEOWNERS](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/CODEOWNERS) for direction pages
* [ ] Update policy used for issues generated in the [Product](https://gitlab.com/gitlab-com/Product/-/blob/main/config/sections.yml) group.
* [ ] Consult EM and UX counterparts on group-specific pages or auto-generated issues that need to be updated. 
  * [ ] Consult EMs to add and remove from Google groups as needed

### Relevant Documents 

* [Add Document](URL)
* [Add Document](URL)
* [Add Document](URL)

### Closing Tasks 

* [ ] Mark all tasks as complete 
* [ ] Update [template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Product_Transition.md) with retrospective thread items
* [ ] Close Issue
