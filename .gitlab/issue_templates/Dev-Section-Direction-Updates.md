## Introduction

After each release it's important to update your category and stage visions to ensure they accurately reflect what was delivered, especially in the `What's Next` sections. You should also consider this an opportunity to make any updates that you might have been considering  but not put the time into creating an MR for. Consider creating a branch and reviewing each of your Direction pages (Stage and Categories) one-by-one looking for updates.

Be sure to follow the most up-to-date guidance in the handbook about [managing your product direction](https://about.gitlab.com/handbook/product/product-processes/#managing-your-product-direction).

Check the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) for the due date for this content.

## Tasks

Submit and merge MRs for any needed updates. If there are updates, post them in the appropriate channels (#s_, #g_)

### :o: Opening Tasks
* [ ] @mushakov - Add Retrospective Thread to this issue 
* [ ] @mushakov  - Assign a due date to this issue

### :telescope: Update Group Categories and Related Themes

For example the direction/verify/continous_integration or direction/monitor/health/incident_management pages.

Note: Before submitting for review or merging yourself, be sure to:
- Consider whether you can separate out controversial and non-controversial updates to focus discussion and feedback on specific changes
- Look at your page in the review app to make sure that it looks like you expect and you don't have markdown errors
- Run a spell check and/or grammar checker
- Open all your links and make sure none go to issues you've already delivered/moved or are otherwise dead links
- Review and update the entire document, not just the what's next section
- For controversial or material updates ping your Stage, Section and Product Leaders to provide a global perspective during review

* [ ] @m_frankiewicz  - Manage:Import & Manage:Integrations- Category visions reviewed
* [ ] @uchetta - Manage:Foundations - Category visions reviewed
* [ ] @gweaver - Plan:Project Management - Category visions reviewed
* [ ] @amandarueda - Plan:Product Planning - Category visions reviewed
* [ ] @hsnir1 - Plan:Optimize - Category visions reviewed
* [ ] @mmacfarlane -  Plan:Knowledge- Category visions reviewed
* [ ] @derekferguson  - Create:Source Code - Category visions reviewed
* [ ] @phikai - Create:Code Review - Category visions reviewed
* [ ] @ericschurter - Create:Editor - Category visions reviewed


### :chart_with_upwards_trend: Review Maturity Plans

Check each category is listed and up to date on the [Maturity](https://about.gitlab.com/direction/maturity/) page.
Category Maturity Plans can be updated by modifying [categories.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/categories.yml).

Reminder: Review the maturity dates for your category and update them if needed.

* [ ] @m_frankiewicz  - Manage:Import & Manage:Integrations - Maturity plans reviewed
* [ ] @gweaver - Plan:Project Management - Maturity plans reviewed
* [ ] @amandarueda - Plan:Product Planning - Maturity plans reviewed
* [ ] @mmacfarlane - Plan:Knowledge - Maturity plans reviewed
* [ ] @hsnir1 - Plan:Optimize - Maturity plans reviewed
* [ ] @derekferguson - Create:Source Code - Maturity plans reviewed
* [ ] @phikai - Create:Code Review - Maturity plans reviewed
* [ ] @ericschurter - Create:Editor - Maturity plans reviewed

### :writing_hand: Update Stage Content
Review stage direction pages including tiering strategy content. Be sure to incorporate appropriate [pricing themes](https://about.gitlab.com/company/pricing/#themes) in that review.  On a quarterly basis consider scheduling a Direction review discussion with Product leaders to solicit a global perspective on your stage Direction.  
* [ ] @uchetta, and @m_frankiewicz - Manage - Stage Direction Updated
* [ ] @mushakov, @hsnir1, @amandarueda, @mmacfarlane and @gweaver - Plan - Stage Direction Updated
* [ ] @derekferguson, @ericschurter and @phikai - Create - Stage Direction Updated

### Update Section Content
Review and update section direction pages including tiering strategy content, update section walk-through
* [ ] @mushakov - Update [Section Direction](https://about.gitlab.com/direction/dev/) page
* [ ] @mushakov - Update Section Walk Through
* [ ] @mushakov - Quarterly - consider scheduling a section-wide direction review and encouraging all Gitlab team members to contribute

### :x: Closing Tasks
* [ ] @mushakov - Review category maturity plans for the next three months
* [ ] @mushakov - Review all updates
* [ ] @mushakov - Post Highlights comment on this issue upon reviewing all updates and communicate widely (ping team, post in slack, ping `@gl-product-leadership`)
* [ ] @mushakov - Follow up on retrospective thread activities with [updates to this template](.gitlab/issue_templates/Dev-Section-Direction-Updates.md). Changes to scheduling in [this](config/issues.yml) file

/assign @hsnir1, @m_frankiewicz, @mushakov, @gweaver, @ericschurter, @phikai ,  @amandarueda, @mmacfarlane, @uchetta, @derekferguson, @steve-evangelista


