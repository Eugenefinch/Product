NOTE: starting 2023-03, Fulfillment is reviewing OKRs in lieu of Product PIs for Fulfillment ([details](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/1088#note_1318891535)).

## :book: References

* [Meeting Notes](https://docs.google.com/document/d/17smuC22Ncu5PP0Ao9QdZnkWK0nbe7ArJveDRp-95AGE/edit)
* [Past Prep Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Afulfillment)

## :white_check_mark: Tasks

### :o: Opening Tasks
* [ ] Set a due date to this issue as 2 business days prior to the scheduled review to make time for Leadership async review - @ofernandez2
* [ ] Add links to top level OKR issues for updates and reviews - @ofernandez2
* [ ] Update the Meeting Notes with the timing and this Prep Issue - @ofernandez2
 
### :heavy_check_mark: Other Tasks
* [ ] Check remaining follow ups from [last review](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Afulfillment) - @ofernandez2

### :star: OKR updates

Each group should update their **OKRs in GitLab** (linked to from the agenda doc), with a focus on ~"priority::1" and ~"priority::2" OKRs first. Make sure to update:
* Progress
* Health status
* Add a comment explaining the progress and health status

#### Group OKR Updates

* [ ] Provision - @courtmeddaugh 
* [ ] Billing and Subscription Management - @tgolubeva
* [ ] Purchase - @courtmeddaugh
* [ ] Utilization - @alex_martin
* [ ] Fulfillment Platform - @tgolubeva 
* [ ] Provision Eng KRs - @isandin 
* [ ] B&SM Eng KRs - @rhardarson
* [ ] Purchase Eng KRs - @shreyasagarwal 
* [ ] Utilization Eng KRs - @csouthard
* [ ] Fulfillment Platform Eng KRs - @jameslopez

### :package: Pre-Meeting Tasks
* [ ] Review and ask questions in the doc ahead of PI review meeting: 
   * [ ] SUS Impacting Issues @jackib
   * [ ] Dev PIs @jeromezng
   * [ ] Quality PIs @vincywilson @chloeliu
   * [ ] OKRs @ofernandez2

### :x: Closing Tasks
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Fulfillment-Section-PI-Review.md) based on the retrospective thread - `@ofernandez2`
* [ ] Add a highlights comment and share in Slack - `@ofernandez2`

/assign @ofernandez2 @courtmeddaugh @tgolubeva @alex_martin @isandin @rhardarson @shreyasagarwal @csouthard @jameslopez @jeromezng @jackib @vincywilson @chloeliu

/label ~"section::fulfillment"  ~"PI Review Prep"
