## Update `features.yml`

`@gl-product-leadership` Please ensure your team's features are up-to-date for the Post Purchase Survey by July 15th.

- [Features by Tier page](https://about.gitlab.com/features/by-paid-tier/)
- [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/features.yml)

Please mention this issue in any resulting MRs.

## ProdOps Tasks

- Previous quarter's `[Feature list Comparison](ADD URL)`

- [ ] Create FYXX-QX PPS Feature list Comparison and add link `[here](ADD LINK)` `@fseifoddini @brhea`
  - Duplicate previous comparison sheet
  - Export questions from Qualtrics
    - Results > Share Report > CSV
  - Add downloaded CSV to duplicated comparison sheet as a new sheet
  - Update column values based on features list on Features by Tier
- [ ] [Update the survey feature lists in Qualtrics](https://www.youtube.com/watch?v=6rBStkOix6w) based on FYXX QX PPS Feature list  `@brhea`
- [ ] Tag `@gl-product-leadership` for action

/label ~"workflow::In dev" ~"Product Operations"
/assign @brhea @fseifoddini
