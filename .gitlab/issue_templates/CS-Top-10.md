# CS Top 10 - (20XX CQY)

* [ ] TAM represenative: Attached final voted issues
* [ ] TAM represenative: Assign to Director of Product, Dev Products or Director of Product, Ops Products
* [ ] TAM represenative: Attend the next Weekly Product Call to discuss the new quarter's issues.
* [ ] Product Dir: Assign a product representative to handle this quarter's product tasks
* [ ] Product: Create `gitlab-org` label of the form `CS Top 10 (YYYYCQ#)` and color `#69D100`
* [ ] Product: Go through each issue in the provided summary from TAM team and:
  * [ ] Add the `~"CS Top 10 (YYYYCQ#)"` label to the issues
  * [ ] Ping the appropriate Product Manager in the issue `@pm this issue has been identifed as a CS Top 10 issue for YYYYCQ#`
    * [ ] Product Manager: Add these items to your categories "top customer issues" sections
