## Overview and Context
<!-- Add context and identify the need - what and why 
     For example: "We need to add UX team members to teams that have greater scope compared to other teams in the subdepartment"
-->

## Current Staffing

 <!-- Highlight the teams affected and number of team members for each - a table is helpful to illustrate
| Team | BE IC |  FE IC | EM | PM | UX | SET |
|------|-------|--------|----|----|----|-----|
| Team 1 |  3  |  4     | 1  | 1  | 1  | 0   |
| Team 2 |  4  |  4     | 1  | 1  | 0  | 0   | 
-->

## Requested for Team Realignment

 <!-- Highlight the diffs of each team, for each stable counterpart - a table is helpful to illustrate
| Team | BE IC |  FE IC | EM | PM | UX | SET |
|------|-------|--------|----|----|----|-----|
| Team 1 |  0  |  0     | 0  | 0  | -1  | 0   |
| Team 2 |  0  |  0     | 0  | 0  | +1 | +1   | 
-->

## DRIs for Teams impacted by the Realignment (Optional)

<!-- Example:
| Team | DRI |
| ------ | ------ |
| Team 1 - UX |  |
| Team 2 - Quality |  |
-->

## Timeline and Tasks

<!-- This includes getting approval from leadership, broadcast comms to teams, on Slack etc

Examples:

## Monday - 2020-08-17
- [x] Update background, timeline and tasks and add transparency content - @DRInamegoeshere
- [x] Review issue and approve content
   - [x] @DRInamegoeshere
   - [x] <more leadership handles etc>

## Tuesday - 2020-08-18
- [x] Inform affected team members on Team 1 - @DRInamegoeshere
- [x] Inform affected team members on Team 2 - @DRInamegoeshere
--> 
## FAQ

<!-- growing list of Q&A as they come in - either in the issue, 1:1s etc -->

## Additional Details

<!-- Optionally add any other details as needed, e.g. AMA to follow>
