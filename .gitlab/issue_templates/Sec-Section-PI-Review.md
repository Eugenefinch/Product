## :book: References

* [Handbook Page](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/sec-section/)
* [Meeting Notes](https://docs.google.com/document/d/1yNUGEwUHl8gayRzZzYQGrWWNDphwAU6C-m1V4HWtgWM/edit)
* [PI .yml file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/main/data/performance_indicators/sec_section.yml)

## :dart: Intent
We are organizing for and regularly reviewing Performance Indicators (PIs) in order to enable a dialog between ourselves (all of R&D) about most effective use of our R&D efforts towards the most impactful improvements.

As a communication tool, Performance Indicators are only as useful as the range of the audience. As a result we should:

* store PI metrics, funnels, growth models, status and next steps handbook first (avoid content only living in slides)
* expose our product groups to PI metrics regularly
* communicate transparently about adjustments to PIs within our groups
* reference PIs in planning issues and prioritization discussions

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Set a due date on this issue as the last business day of the week prior to the scheduled review - @sam.white (odd months) or @sarahwaldner (even months)
* [ ] Add a restrospective thread to this issue - @sam.white (odd months) or @sarahwaldner (even months)
* [ ] Add stub MR for PI updates - @sam.white (odd months) or @sarahwaldner (even months)
* [ ] Prep meeting agenda - @sam.white (odd months) or @sarahwaldner (even months)

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Performance Indicators YAML file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/main/data/performance_indicators/sec_section.yml) with:

* Update your `Health:` and `Instrumentation:` levels
* Update any `URLs:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update any `URLs:` to reference additional Sisense dashboards for more details
* Update `Implementation Status:` with the current maturity
* Update `Lessons Learned:` with your takeaways from the previous month's data
* Update `Focus This Month:` with your plan for the month ahead 

### :star: Expectations
1. Ensure your graph is refreshed
1. Ensure your graph looks correct and if it's not, understand why and change it
1. Have a comprehensive understanding of your graphs (understand history, challenges with the data, etc) and document directly in the .yml file
1. Ensure there are no graph labeling or title errors
1. Ensure you have a target set and you know how that target was set. If you can't have a target, in the target field, please explain why you don't have a target and when you would expect to have one.

### :star: Trend Review
1. Review [Prioritization dashboard](https://app.periscopedata.com/app/gitlab/1042933/Issue-Types-by-Milestone) & [MR type dashboard](https://app.periscopedata.com/app/gitlab/976854/Merge-Request-Types-Detail) filtered for team_group on:
    - Composition Analysis
    - Compliance
    - Dynamic Analysis
    - Security Policies
    - Static Analysis
    - Threat Insights
    - Vulnerability research
    - Authentication
    - Authorization
1. Answer questions in monthly meeting agenda regarding dashboard data accuracy, error budgets, and prioritization trends

#### Group Updates

**Sec Section**
* [ ] Section CMAU - @hbenson
* [ ] Section Dev trend review - @wayne, after content written by 
  * [ ] @pcalder
  * [ ] @twoodham
* [ ] Section PD trend review - @jmandell 
* [ ] Section Quality trend review - @vincywilson 

**Secure**
* [ ] Overall Secure Review - @sarahwaldner 
* [ ] Static Analysis - @connorgilbert
* [ ] Dynamic Analysis - @smeadzinger
* [ ] Composition Analysis - @smeadzinger

**Govern**
* [ ] Overall Govern Review - @sam.white 
* [ ] Anti-Abuse - @jstava 
* [ ] Authentication - @hsutor
* [ ] Authorization - @hsutor 
* [ ] Compliance - @jstava
* [ ] Security Policies - @g.hickman
* [ ] Threat Insights - @abellucci

### :x: Closing Tasks
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Sec-Section-PI-Review.md) based on the retrospective thread - @sam.white (odd months) or @sarahwaldner (even months)
* [ ] Activate on any retrospective thread items - @sam.white (odd months) or @sarahwaldner (even months)


/assign @sarahwaldner @smeadzinger @connorgilbert @tauriedavis @jmandell @sam.white @abellucci @g.hickman @jstava @hsutor
